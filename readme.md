# test

## Project setup
```
composer install
```

###  migrate Db 
```
php artisan migrate 
```

### Run Passport command 
```
php artisan passport:install  && php artisan key:generate 
```

### For storage (image ) run this commands 
```
php artisan storage:link
```

### project run by 
```
php artisan serve 
```


